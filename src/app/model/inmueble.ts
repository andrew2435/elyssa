export class Inmueble {
    id: string;
    nombre: string;
    barrio: string;
    direccion: string;
    updated_at: Date;
}
