import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
// import { ComponentsModule } from 'src/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    // ComponentsModule
  ],
  declarations: [PagesComponent]
})
export class PagesModule { }
