import { Component, ViewEncapsulation } from '@angular/core';
// import { MenuController, ViewController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
// import { DeviceUtilsService } from '../../services/device-utils.service';
// import { ErrorToastComponent } from 'src/components/error-toast/error-toast.component';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginPage {

  private static readonly TEXTS = {
    DEFAULT_ERROR: 'ERRORS.SIGN_IN.DEFAULT_ERROR',
    INVALID_EMAIL: 'ERRORS.AUTH.INVALID_EMAIL',
    PASSWORD_REQUIRED: 'ERRORS.AUTH.PASSWORD_REQUIRED',
    PASSWORD_MIN_LENGTH: 'ERRORS.SIGN_IN.PASSWORD_MIN_LENGTH',
    PASSWORD_NUMBER: 'ERRORS.SIGN_IN.PASSWORD_NUMBER',
    PASSWORD_UPPERCASE: 'ERRORS.SIGN_IN.PASSWORD_UPPERCASE',
    PASSWORD_LOWERCASE: 'ERRORS.SIGN_IN.PASSWORD_LOWERCASE',
    PASSWORD_SPECIAL_CHARACTER: 'ERRORS.AUTH.PASSWORD_SPECIAL_CHARACTER'
  };

  /**
   * Object that represents the form
   * to validate email input and password
   */
  signinForm = null;
  loading = false;
  texts: any = {};

  constructor(
    // private menu: MenuController,
    // private errorToastCtrl: ErrorToastComponent,
    // private deviceUtilsService: DeviceUtilsService,
    // private viewCtrl: ViewController,
    private router: Router,
    private authService: AuthenticationService
  ) {
    this.signinForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        // tslint:disable-next-line:max-line-length
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]),
      password: new FormControl('', Validators.required)
    });
  }

  ionViewDidLoad() {
    // this.viewCtrl.setBackButtonText('');
  }

  ionViewWillEnter() {
    // this.deviceUtilsService.unlock();
    // this.menu.enable(false);
  }

  ionViewWillLeave() {
    // this.deviceUtilsService.unlock();
    // this.menu.enable(true);
  }

  /**
   * This function will be call when user clicks on
   * 'Sigin' button. It will validate your email format
   * and password before sending singin with ther account.
   * After signin successfullt the user will be redirected
   * to DashboardPage
   */
  onSignin({ username, password }) {
    const email = username.value.toLowerCase();
    const pass = password.value;

    if (!username.valid) {
      return this.showErrorMessage(this.texts['INVALID_EMAIL']);
    }

    if (!password.valid) {
      return this.showErrorMessage(this.texts['PASSWORD_REQUIRED']);
    }

    this.loading = true;

    this.authService.login().then((data) => {
      this.loading = false;
    });
  }

  private isBadCredentials(error) {
    // const code = lodash.get(error, 'errors[0].code', 0);
    // 104: Invalid user credentials | 117: Number of login attempts exceeded
    // return code === 104 || code === 117;
  }

  /**
   * This message will appear only
   * for error messages
   */
  showErrorMessage(msg) {
    // this.loading = false;
    // const toast = this.errorToastCtrl.create({
    //   message: msg,
    //   duration: 3000
    // });
    // toast.present();
  }
}
