import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Inmueble } from '../model/inmueble';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrl = '/api/v1/products';
@Injectable({
  providedIn: 'root'
})

export class ApiService {
  constructor(private http: HttpClient) {
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getInmueble(id): Observable<Inmueble> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Inmueble>(url).pipe(
      tap(_ => console.log(`fetched product id=${id}`)),
      catchError(this.handleError<Inmueble>(`getProduct id=${id}`))
    );
  }

  addInmueble(product): Observable<Inmueble> {
    return this.http.post<Inmueble>(apiUrl, product, httpOptions).pipe(
      tap((product: Inmueble) => console.log(`added product w/ id=${product.id}`)),
      catchError(this.handleError<Inmueble>('addProduct'))
    );
  }

  updateInmueble(id, product): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, product, httpOptions).pipe(
      tap(_ => console.log(`updated product id=${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteInmueble(id): Observable<Inmueble> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Inmueble>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted product id=${id}`)),
      catchError(this.handleError<Inmueble>('deleteProduct'))
    );
  }
}
