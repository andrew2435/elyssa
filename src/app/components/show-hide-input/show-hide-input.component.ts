import { Component, Output, ViewEncapsulation, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-show-hide-input',
  templateUrl: 'show-hide-input.component.html',
  styleUrls: ['show-hide-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ShowHideInputComponent),
      multi: true
    }
  ]
})
export class ShowHideInputComponent implements ControlValueAccessor {
  private isHidden = true;
  private innerValue: any = '';
  private propagateChange = (_: any) => { };

  @Output()
  onFocus: Subject<any> = new Subject<any>();

  @Output()
  onBlur: Subject<any> = new Subject<any>();

  getType() {
    return (this.isHidden) ? 'password' : 'text';
  }

  getIcon() {
    return (this.isHidden) ? 'eye-off' : 'eye';
  }

  get value(): any {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
    }
  }

  onChange(e: Event, value: any) {
    this.onBlur.next(true);
    this.innerValue = value;
    this.propagateChange(this.innerValue);
  }

  writeValue(value: any): void {
    this.innerValue = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }
}
