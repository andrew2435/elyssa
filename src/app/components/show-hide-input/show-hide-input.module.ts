import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ShowHideInputComponent } from './show-hide-input.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  declarations: [
    ShowHideInputComponent
  ],
  exports: [
    ShowHideInputComponent
  ]
})
export class ShowHideInputModule { }
