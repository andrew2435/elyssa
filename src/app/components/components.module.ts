import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ShowHideInputModule } from './show-hide-input/show-hide-input.module';
import { ShowHideInputComponent } from './show-hide-input/show-hide-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot(),
  ],
  exports: [
    ShowHideInputComponent
  ],
  declarations: [
    ShowHideInputComponent
  ],
  entryComponents: [
    ShowHideInputComponent
  ],
})
export class ComponentsModule { }
